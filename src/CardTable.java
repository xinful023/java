import java.util.ArrayList;
import java.util.ListIterator;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/* 
 * 4 Cards from the deck will be placed to each player and 8 cards will be placed in the pool.
 * 
 * Player can click and drag the card from hand to the pool. If the sum of the two cards' face 
 * values equals 14 (ace = 1 ... king = 13), the player will get the card from the pool. New cards 
 * will be placed.
 * 
 * The name of the player will be bold if the player is leading in the game.
 */

public class CardTable extends JComponent implements MouseListener, MouseMotionListener {
  private static final Color BACKGROUND_COLOR = Color.GREEN.darker();
  private static final int TABLE_SIZE = 750;
  private static final int INTER = 30; // interval size between two cards
  
  private int _cw;
  private int _ch;
  
  private ArrayList<Card> _deck = new ArrayList<Card>();
  private ArrayList<Card> _pool = new ArrayList<Card>();
  
  private Player _player1;
  private Player _player2;
  
  private Card _currentCard = null;
  private Card _c0 = null;
  private int _pos;
  
  private int _initX;
  private int _initY;
  private int _dragFromX;
  private int _dragFromY;
  
  public CardTable(ArrayList<Card> deck, Player player1, Player player2) {
    _deck = deck;
    _cw = _deck.get(0).getWidth();
    _ch = _deck.get(0).getHeight(); // get card dimensions
    
    _player1 = player1;
    _player2 = player2;
    int num = 4; // number of cards in each player's hand
    
    int w = num * _deck.get(0).getWidth() + (num - 1) * INTER;
    setPreferredSize(new Dimension(w, TABLE_SIZE)); // set table dimensions
    
    _pos = (w - INTER * (num - 1) - _cw)/2;
    
    for(int i = 0; i < num; i++) { // initialize cards for each player
      _c0 = _deck.remove(0);
      _player1.cards.add(_c0);
      
      _c0 = _deck.remove(0);
      _player2.cards.add(_c0);
    }
    _player1.arrangeCards(_pos, 0, INTER);
    _player2.arrangeCards(_pos, TABLE_SIZE - _ch, INTER);
    
    for(int i = 0; i < num; i++) { // initialize cards in the pool
      _c0 = _deck.remove(0);
      _c0.moveTo((_c0.getWidth() + INTER) * i, TABLE_SIZE / 2 - _ch - INTER);
      _pool.add(_c0);
      
      _c0 = _deck.remove(0);
      _c0.moveTo((_cw + INTER) * i, TABLE_SIZE / 2 + INTER);
      _pool.add(_c0);
    }
    
    addMouseListener(this);
    addMouseMotionListener(this);
  }
  
  @Override
  public void paintComponent(Graphics g) {
    g.setColor(BACKGROUND_COLOR);
    g.fillRect(0, 0, getWidth(), getHeight());
    
    g.setColor(Color.BLACK);
    g.setFont(new Font("TimesRoman", Font.BOLD, 20));
    String t = "Fishing Card Game"; 
    g.drawString(t, (getWidth() - g.getFontMetrics().stringWidth(t))/2, 
        (getHeight() + g.getFontMetrics().getHeight()*3/4)/2);
    
    g.setFont(new Font("TimesRoman", Font.PLAIN, 15));
    if(_player1.getScore() > _player2.getScore()) {
      g.setFont(new Font("TimesRoman", Font.BOLD, 15));
    }
    
    _player1.moveTo(0, _ch + INTER);
    _player1.draw(g, this);
    
    g.setFont(new Font("TimesRoman", Font.PLAIN, 15));
    if(_player2.getScore() > _player1.getScore()) {
      g.setFont(new Font("TimesRoman", Font.BOLD, 15));
    }
    _player2.moveTo(0, TABLE_SIZE - _ch - _player2.getHeight() - INTER);
    _player2.draw(g, this);
    
    for(Card c : _pool) {
      c.draw(g, this);
    }
    
    for(Card c : _player1.cards) {
      c.draw(g, this);
    }
    
    for(Card c : _player2.cards) {
      c.draw(g, this);
    }
  }
  
  @Override
  public void mousePressed(MouseEvent e) {
    int x = e.getX();
    int y = e.getY();
    _currentCard = null;
    
    ListIterator<Card> li1 = _player1.cards.listIterator(_player1.cards.size());
    while(li1.hasPrevious()) { // search for the card clicked in player1's hand in reverse order
      Card testCard = li1.previous();
      if(testCard.contains(x, y)) {
        _initX = testCard.getX();
        _initY = testCard.getY();
        _dragFromX = x - _initX;
        _dragFromY = y - _initY;
        _currentCard = testCard;
        break;
      }
    }
    
    ListIterator<Card> li2 = _player2.cards.listIterator(_player2.cards.size());
    while(li2.hasPrevious()) {// search for the card clicked in player2's hand in reverse order
      Card testCard = li2.previous();
      if(testCard.contains(x, y)) {
        _initX = testCard.getX();
        _initY = testCard.getY();
        _dragFromX = x - _initX;
        _dragFromY = y - _initY;
        _currentCard = testCard;
        break;
      }
    }
  }
  
  @Override
  public void mouseDragged(MouseEvent e) {
    if(_currentCard != null) {
      int newX = e.getX() - _dragFromX;
      int newY = e.getY() - _dragFromY;
      
      newX = Math.max(newX, 0);
      newX = Math.min(newX, getWidth() - _currentCard.getWidth());
      
      newY = Math.max(newY, 0);
      newY = Math.min(newY, getHeight() - _currentCard.getHeight());
      
      _currentCard.moveTo(newX, newY);
      
      this.repaint();
    }
  }
  
  @Override
  public void mouseReleased(MouseEvent e) {
    if(_currentCard != null) {
      for(Card c : _pool) {
        int p0 = c.getX();
        int q0 = c.getY();
        int p1 = c.getX() + _cw;
        int q1 = c.getY() + _ch;
        
        if((_currentCard.contains(p0, q0) || _currentCard.contains(p1, q0) 
            || _currentCard.contains(p0, q1) || _currentCard.contains(p1, q1)) 
            && (_currentCard.getRank() + c.getRank()) == 12) { // check if the pair is valid
          _currentCard._on = false;
          c._on = false;
          
          if(_player1.cards.contains(_currentCard)) {
            if(_deck.size() != 0) {
              _player1.cards.add(c);
              _c0 = _deck.remove(0);
              _player1.cards.add(_c0);
              _player1.arrangeCards(_pos, 0, INTER);
            }
          }
          
          if(_player2.cards.contains(_currentCard)) {
            if(_deck.size() != 0) {
              _player2.cards.add(c);
              _c0 = _deck.remove(0);
              _player2.cards.add(_c0);
              _player2.arrangeCards(_pos, TABLE_SIZE - _ch, INTER);
            }
          }
          
          if(_deck.size() != 0) {
            _c0 = _deck.remove(0);
            _c0.moveTo(c.getX(), c.getY());
            _pool.add(_c0);
          }
          
          this.repaint();
          
          return;
        }
      }
      
      _currentCard.moveTo(_initX, _initY);
      
      this.repaint();
    }
  }
  
  @Override
  public void mouseExited(MouseEvent e) {
    if(_currentCard != null) {
      _currentCard.moveTo(_initX, _initY);
    }
  }
  
  public void mouseClicked(MouseEvent e) {}
  public void mouseEntered(MouseEvent e) {}
  public void mouseMoved(MouseEvent e) {}
}
