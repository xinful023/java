import java.awt.*;
import javax.swing.*;

public class Card {
  private int _rank;
  private char _suit;
  private int _x;
  private int _y;
  private ImageIcon _image;
  public boolean _on; // draw this card or not
  
  public Card(int r, char s, ImageIcon image) {
    _rank = r;
    _suit = s;
    _image = image;
    _on = true;
  }
  
  public void moveTo(int x, int y) {
    _x = x;
    _y = y;
  }
  
  public boolean contains(int x, int y) {
    return (x >= _x && x <= (_x + getWidth()) && y >= _y && y <= (_y + getHeight()));
  }
  
  public int getWidth() {
    return _image.getIconWidth();
  }
  
  public int getHeight() {
    return _image.getIconHeight();
  }
  
  public int getX() {
    return _x;
  }
  
  public int getY() {
    return _y;
  }
  
  public char getSuit() {
    return _suit;
  }
  
  public int getRank() {
    return _rank;
  }
  
  public void draw(Graphics g, Component c) {
    if(_on) {
      _image.paintIcon(c, g, _x, _y);
    }
  }
}
