import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.awt.*;
import javax.swing.*;

public class Player {
  private String _name;
  private ImageIcon _image;
  private int _x;
  private int _y;
  private int _w = 40; // player profile image width
  private int _h = 40; // player profile image height
  public ArrayList<Card> cards = new ArrayList<Card>();
  private boolean _s;
  
  Player(String name, ImageIcon image) {
    _name = name;
    _image = image;
  }
  
  public void moveTo(int x, int y) {
    _x = x;
    _y = y;
  }
  
  public int getX() {
    return _x;
  }
  
  public int getY() {
    return _y;
  }
  
  public int getWidth() {
    return _w;
  }
  
  public int getHeight() {
    return _h;
  }
  
  public void draw(Graphics g, Component c) {
    Image img = _image.getImage();
    Image newimg = img.getScaledInstance(_w, _h, java.awt.Image.SCALE_SMOOTH);
    _image = new ImageIcon(newimg);
    _image.paintIcon(c, g, _x, _y);
    
    g.drawString('\t' + this._name + ": " + Integer.toString(this.getScore()), this._x + _w, 
        this._y + _h/2);
  }
  
  public int getScore() {
    Dictionary<Character, Integer> dict = new Hashtable<Character, Integer>(); // scoring rule
    dict.put('s', 4); // spade - 4 pts
    dict.put('h', 3); // heart - 3 pts
    dict.put('c', 2); // club - 2 pts
    dict.put('d', 1); // diamond - 1 pts
    
    int r = 0;
    for(Card c : cards) {
      r += dict.get(c.getSuit());
    }
    
    return r;
  }
  
  public void arrangeCards(int x, int y, int i) {
    int j = 0;
    for(Card c: cards) {
      if(c._on) {
        c.moveTo(x + j * i, y);
        j += 1;
      }
    }
  }
}
